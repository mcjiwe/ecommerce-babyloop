// React
import React, { useState, ChangeEvent, useEffect } from 'react';

// Router
import { RouteComponentProps } from 'react-router';

// Material UI
import { withStyles, WithStyles } from '@material-ui/styles';

// Styles
import styles from './Home.styles';
import { SessionContainer } from '../../stores/SessionStore';
import { ProductProps } from '../../models/Product';
import {
  Grid,
  TextField,
  Typography,
  Button,
  CircularProgress
} from '@material-ui/core';
import { Search, ShoppingCart, Close, Delete } from '@material-ui/icons';

// Components
import { Dropdown } from '../../components';

// Models
import { FiltersProps } from '../../models/Filters';
import { DropdownProps } from '../../models/Dropdown';

// Images
import Logo from '../../assets/Logo.png';

type Props = WithStyles<typeof styles> & RouteComponentProps;

interface State {
  loading: boolean;
  products: Array<ProductProps>;
  categories: Array<DropdownProps>;
  subCategories: Array<DropdownProps>;
  brands: Array<DropdownProps>;
  filters: FiltersProps;
  showCart: boolean;
}

const Home = ({ classes, ...props }: Props): JSX.Element => {
  const [state, setState] = useState<State>({
    loading: true,
    products: [],
    categories: [],
    subCategories: [],
    brands: [],
    filters: {
      search: '',
      categoryId: 0,
      subCategoryId: 0,
      orderBy: 0,
      brandId: 0
    },
    showCart: false
  });

  const session = SessionContainer.useContainer();

  useEffect(() => {
    async function getProducts(): Promise<void> {
      const products = await session.getProducts();
      const categories: Array<DropdownProps> = [];
      const subCategories: Array<DropdownProps> = [];
      const brands: Array<DropdownProps> = [];

      products.map(product => {
        product.base_product.categories.map(
          categorie =>
            !categories.find(c => c.id === categorie.id) &&
            categories.push({ value: categorie.name, id: categorie.id })
        );

        product.base_product.sub_categories.map(
          subCategorie =>
            !subCategories.find(c => c.id === subCategorie.id) &&
            subCategories.push({
              value: subCategorie.name,
              id: subCategorie.id
            })
        );

        if (!brands.find(brand => brand.id === product.base_product.brand.id)) {
          brands.push({
            value: product.base_product.brand.name,
            id: product.base_product.brand.id
          });
        }

        return undefined;
      });

      setState(prevState => ({
        ...prevState,
        loading: false,
        products,
        categories,
        subCategories,
        brands
      }));
    }
    getProducts();
  }, [session]);

  const handleChange = (event: ChangeEvent<any>): void => {
    const { name, value } = event.target;
    setState(prevState => ({
      ...prevState,
      filters: { ...prevState.filters, [name]: value }
    }));
  };

  const getFilteredProducts = (): Array<ProductProps> => {
    const regexp = new RegExp(state.filters.search, 'gi');
    const productsCopy: Array<ProductProps> = state.products;

    switch (state.filters.orderBy) {
      case 0:
        productsCopy.sort((a, b) =>
          new Date(a.created_at) < new Date(b.created_at) ? 1 : -1
        );
        break;
      case 1:
        productsCopy.sort((a, b) =>
          a.base_product.name > b.base_product.name ? 1 : -1
        );
        break;
      case 2:
        productsCopy.sort((a, b) =>
          a.base_product.name < b.base_product.name ? 1 : -1
        );
        break;
      case 3:
        productsCopy.sort((a, b) =>
          parseFloat(a.price) > parseFloat(b.price) ? 1 : -1
        );
        break;
      case 4:
        productsCopy.sort((a, b) =>
          parseFloat(a.price) < parseFloat(b.price) ? 1 : -1
        );
        break;
    }

    return productsCopy
      .filter(product => product.base_product.name.search(regexp) !== -1)
      .filter(product =>
        state.filters.categoryId !== 0
          ? product.base_product.categories.find(
              categorie => categorie.id === state.filters.categoryId
            )
          : true
      )
      .filter(product =>
        state.filters.subCategoryId !== 0
          ? product.base_product.sub_categories.find(
              categorie => categorie.id === state.filters.subCategoryId
            )
          : true
      )
      .filter(product =>
        state.filters.brandId !== 0
          ? product.base_product.brand.id === state.filters.brandId
          : true
      );
  };

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid container item xs justify="center" className={classes.header}>
          <Grid item xs>
            <img
              src={Logo}
              height={50}
              className={classes.headerLogo}
              onClick={(): void => props.history.push('/')}
              alt="Logo"
            />
          </Grid>
          <Grid container item spacing={4} xs={8}>
            <Grid item xs></Grid>
            <Grid item xs={8}>
              <TextField
                name="search"
                value={state.filters.search}
                onChange={handleChange}
                className={classes.textInput}
                fullWidth
                InputProps={{
                  disableUnderline: true,
                  startAdornment: <Search className={classes.searchAdornment} />
                }}
              />
            </Grid>
            <Grid
              container
              item
              xs
              alignItems="center"
              justify="flex-end"
              style={{ position: 'relative' }}
            >
              <div style={{ position: 'relative' }}>
                <ShoppingCart
                  onClick={(): void =>
                    setState(prevState => ({
                      ...prevState,
                      showCart: !prevState.showCart
                    }))
                  }
                  style={{ cursor: 'pointer' }}
                />
                {session.cart.products.length > 0 && (
                  <div className={classes.shoppingCartBubble}>
                    <Typography className={classes.shoppingCartBubbleText}>
                      {session.cart.products.length}
                    </Typography>
                  </div>
                )}
              </div>
              {state.showCart && (
                <Grid
                  container
                  item
                  spacing={2}
                  className={classes.shoppingCart}
                >
                  {session.cart.products.length > 0 && (
                    <Grid container item xs={12} className={classes.timer}>
                      <Typography>
                        Tem{' '}
                        {`${new Date(
                          session.cart.countdown
                        ).getMinutes()}:${new Date(session.cart.countdown)
                          .getSeconds()
                          .toString()
                          .padStart(2, '0')}`}{' '}
                        para efetuar a compra.
                      </Typography>
                    </Grid>
                  )}
                  <Grid container justify="space-between" item>
                    <Grid item>Carrinho de compras</Grid>
                    <Grid item>
                      <Close
                        onClick={(): void =>
                          setState(prevState => ({
                            ...prevState,
                            showCart: !prevState.showCart
                          }))
                        }
                        style={{ cursor: 'pointer' }}
                      />
                    </Grid>
                  </Grid>
                  <Grid container item spacing={4}>
                    {session.cart.products.map(product => (
                      <Grid
                        key={product.id}
                        container
                        item
                        spacing={1}
                        className={classes.shoppingCartProduct}
                      >
                        <Grid item>
                          <img
                            src={product.product_pictures[0].picture.url}
                            width={100}
                            alt={product.base_product.name}
                          />
                        </Grid>
                        <Grid container item xs direction="column">
                          <Grid item>
                            <Typography className={classes.text}>
                              {product.base_product.name}
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography className={classes.text}>
                              {product.base_product.brand.name}
                            </Typography>
                          </Grid>
                        </Grid>
                        <Grid container item xs justify="flex-end">
                          <div
                            style={{
                              display: 'flex',
                              flexDirection: 'column',
                              textAlign: 'end'
                            }}
                          >
                            <Typography className={classes.price}>
                              {product.price}
                            </Typography>
                            <Typography className={classes.priceRazored}>
                              {product.base_product.market_price}€
                            </Typography>
                            <Delete
                              onClick={session.removeProductFromCart(product)}
                              className={classes.shoppingCartDeleteButton}
                            />
                          </div>
                        </Grid>
                      </Grid>
                    ))}
                  </Grid>
                  <Grid container item justify="space-between">
                    <Grid item>
                      <Typography className={classes.text}>Total</Typography>
                    </Grid>
                    <Grid item>
                      <Typography className={classes.text}>
                        {session.cart.total.toFixed(2)}€
                      </Typography>
                    </Grid>
                  </Grid>
                  <Grid item xs>
                    <Button
                      className={classes.shoppingCartButton}
                      onClick={(): void => props.history.push('/shopping-cart')}
                    >
                      <Typography className={classes.text}>
                        Finalizar compra
                      </Typography>
                    </Button>
                  </Grid>
                </Grid>
              )}
            </Grid>
            <Grid
              container
              item
              spacing={2}
              alignItems="center"
              justify="flex-end"
            >
              <Grid item className={classes.headerFilter}>
                <Typography className={classes.headerFilterText}>
                  Categoria:
                </Typography>
                <Dropdown
                  name="categoryId"
                  placeholder="Todas"
                  options={state.categories}
                  selectedOption={
                    state.filters.categoryId ? state.filters.categoryId : 0
                  }
                  handleChange={handleChange}
                />
              </Grid>
              <Grid item className={classes.headerFilter}>
                <Typography className={classes.headerFilterText}>
                  Marca:
                </Typography>
                <Dropdown
                  name="brandId"
                  placeholder="Todas"
                  options={state.brands}
                  selectedOption={
                    state.filters.brandId ? state.filters.brandId : 0
                  }
                  handleChange={handleChange}
                />
              </Grid>
              <Grid item className={classes.headerFilter}>
                <Typography className={classes.headerFilterText}>
                  Ordenar por:
                </Typography>
                <Dropdown
                  name="orderBy"
                  placeholder="Mais recentes"
                  options={[
                    { value: 'Produtos A-Z', id: 1 },
                    { value: 'Produtos Z-A', id: 2 },
                    { value: 'Preço ascendente', id: 3 },
                    { value: 'Preço descendente', id: 4 }
                  ]}
                  selectedOption={
                    state.filters.orderBy ? state.filters.orderBy : 0
                  }
                  handleChange={handleChange}
                />
              </Grid>
              <Grid item className={classes.headerFilter}>
                <Typography className={classes.headerFilterText}>
                  Tag:
                </Typography>
                <Dropdown
                  name="subCategoryId"
                  placeholder="Todas"
                  options={state.subCategories}
                  selectedOption={
                    state.filters.subCategoryId
                      ? state.filters.subCategoryId
                      : 0
                  }
                  handleChange={handleChange}
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs></Grid>
        </Grid>
        <Grid container item justify="center">
          <Grid container item xs={8} spacing={4} style={{ marginTop: 10 }}>
            {state.loading ? (
              <div className={classes.loading}>
                <CircularProgress />
              </div>
            ) : (
              getFilteredProducts().map(product => (
                <Grid key={product.id} container item xs={3}>
                  <Grid item>
                    <img
                      src={product.product_pictures[0].picture.url}
                      style={{ width: '100%', cursor: 'pointer' }}
                      alt={product.base_product.name}
                      onClick={(): void =>
                        props.history.push(`/product/${product.id}`)
                      }
                    />
                  </Grid>
                  <Grid item>
                    <Typography className={classes.text}>
                      {product.base_product.name}
                    </Typography>
                    <Typography className={classes.text}>
                      {product.base_product.categories[0].name}
                    </Typography>
                  </Grid>
                  <Grid item className={classes.priceWrapper}>
                    <Typography className={classes.price}>
                      {product.price}€
                    </Typography>
                    <Typography className={classes.priceRazored}>
                      {product.base_product.market_price}€
                    </Typography>
                  </Grid>
                  <Grid container item justify="center">
                    <Button
                      className={[
                        classes.addButton,
                        session.cart.products.find(p => p.id === product.id)
                          ? classes.addedButton
                          : undefined
                      ].join(' ')}
                      onClick={
                        !state.showCart
                          ? !session.cart.products.find(
                              p => p.id === product.id
                            )
                            ? session.addProductToCart(product)
                            : session.removeProductFromCart(product)
                          : undefined
                      }
                    >
                      <Typography className={classes.text}>
                        {session.cart.products.find(p => p.id === product.id)
                          ? 'Adicionado ao carrinho'
                          : 'Adicionar ao carrinho'}
                      </Typography>
                    </Button>
                  </Grid>
                </Grid>
              ))
            )}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(Home);
