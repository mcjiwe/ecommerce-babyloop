import { createStyles } from '@material-ui/core';

export default createStyles({
  root: {
    height: '100vh',
    width: '100vw',
    overflow: 'hidden auto'
  },
  text: {
    fontSize: 'calc(10px + 0.5vmin)'
  },
  loading: {
    height: 200,
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    padding: 5
  },
  headerLogo: {
    marginLeft: 20,
    cursor: 'pointer'
  },
  headerFilterText: {
    minWidth: 'fit-content',
    fontSize: 'calc(10px + 0.5vmin)',
    textAlign: 'right',
    '@media only screen and (max-width: 1000px)': {
      minWidth: '80px'
    }
  },
  headerFilter: {
    width: 'calc(100% / 4)',
    maxWidth: 'fit-content',
    height: 50,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    '@media only screen and (max-width: 1000px)': {
      width: '50%'
    }
  },
  shoppingCart: {
    position: 'absolute',
    padding: 20,
    width: 'max-content',
    backgroundColor: 'white',
    maxWidth: 500,
    top: 70,
    right: 8,
    zIndex: 1300,
    '-webkit-box-shadow': '0 0 5px 0 rgba(0,0,0,0.63)',
    '-moz-box-shadow': '0 0 5px 0 rgba(0,0,0,0.63)',
    boxShadow: '0 0 5px 0 rgba(0,0,0,0.63)'
  },
  shoppingCartBubble: {
    position: 'absolute',
    top: -5,
    right: -5,
    backgroundColor: 'lightpink',
    height: 15,
    width: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%'
  },
  shoppingCartBubbleText: {
    fontWeight: 'bold',
    fontSize: 'calc(6px + 0.5vmin)',
    color: 'black'
  },
  shoppingCartProduct: {
    border: '1px solid pink',
    padding: 10,
    borderRadius: 10,
    margin: 10
  },
  shoppingCartButton: {
    width: '100%',
    borderRadius: 20,
    color: 'white',
    backgroundColor: 'black',
    fontSize: 'calc(10px + 0.5vmin)',
    '&:hover': {
      backgroundColor: 'black'
    }
  },
  shoppingCartDeleteButton: {
    alignSelf: 'flex-end',
    cursor: 'pointer'
  },
  textInput: {
    border: '1px solid black',
    borderRadius: 5,
    height: 40,
    '& .MuiInput-root': {
      height: '100%'
    }
  },
  searchAdornment: {
    paddingLeft: 10
  },
  priceWrapper: {
    marginTop: '10px',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  },
  price: {
    fontSize: 'calc(14px + 0.5vmin)',
    fontWeight: 'bold'
  },
  priceRazored: {
    paddingLeft: 10,
    fontSize: 'calc(10px + 0.5vmin)',
    textDecoration: 'line-through'
  },
  addButton: {
    borderRadius: 20,
    backgroundColor: 'pink',
    '&:hover': {
      backgroundColor: 'lightpink'
    },
    '& p': {
      fontSize: 'calc(10px + 0.5vmin)'
    }
  },
  addedButton: {
    backgroundColor: 'deeppink',
    '&:hover': {
      backgroundColor: 'hotpink'
    }
  },
  timer: {
    backgroundColor: 'lightpink',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  }
});
