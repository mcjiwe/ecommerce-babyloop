// React
import React, { useEffect, useState } from 'react';

// Router
import { RouteComponentProps } from 'react-router';

// Material UI
import { withStyles, WithStyles } from '@material-ui/styles';
import { Grid, Typography, CircularProgress, Button } from '@material-ui/core';

// Styles
import styles from './ProductInfo.styles';

// Images
import Logo from '../../assets/Logo.png';

// Stores
import { SessionContainer } from '../../stores/SessionStore';

// Models
import { ProductProps } from '../../models/Product';
import { ArrowLeft } from '@material-ui/icons';

type Props = WithStyles<typeof styles> & RouteComponentProps<{ id: string }>;

interface State {
  loading: boolean;
  product?: ProductProps;
  selectedPicture: number;
}

const ProductInfo = ({ classes, ...props }: Props): JSX.Element => {
  const session = SessionContainer.useContainer();
  const [state, setState] = useState<State>({
    loading: true,
    selectedPicture: 0
  });

  useEffect(() => {
    async function getProduct(): Promise<void> {
      const product: ProductProps = await session.getProduct(
        parseInt(props.match.params.id, 10)
      );
      setState(prevState => ({ ...prevState, loading: false, product }));
    }
    getProduct();
  }, [props.match.params, session]);

  const setPicture = (selectedPicture: number) => (): void => {
    setState(prevState => ({ ...prevState, selectedPicture }));
  };

  return (
    <div className={classes.root}>
      <Grid container justify="center">
        <Grid item xs={12} className={classes.header}>
          <img
            src={Logo}
            height={50}
            className={classes.headerLogo}
            onClick={(): void => props.history.push('/')}
            alt="Logo"
          />
        </Grid>
        <Grid container item alignItems="center">
          <ArrowLeft />
          <Typography
            className={classes.text}
            onClick={(): void => props.history.push('/')}
            style={{ cursor: 'pointer', textDecoration: 'underline' }}
          >
            {' '}
            Voltar para trás
          </Typography>
        </Grid>
        {state.loading ? (
          <Grid
            container
            item
            alignItems="center"
            justify="center"
            style={{ height: 200 }}
          >
            <CircularProgress />
          </Grid>
        ) : (
          state.product && (
            <Grid
              container
              item
              xs={8}
              justify="center"
              spacing={4}
              className={classes.productInfoContainer}
            >
              <Grid item xs={5}>
                <img
                  src={
                    state.product.product_pictures[state.selectedPicture]
                      .picture.url
                  }
                  className={classes.image}
                  alt={state.product.base_product.name}
                />
                {state.product.product_pictures.map((product, index) => (
                  <img
                    key={state.product && product.picture.url}
                    src={product.picture.url}
                    className={classes.thumbnail}
                    onClick={setPicture(index)}
                    alt={state.product && state.product.base_product.name}
                  />
                ))}
              </Grid>
              <Grid container item xs={5} direction="column">
                <Grid container item direction="column">
                  <Typography className={classes.productInfoTitle}>
                    {state.product.base_product.name}
                  </Typography>
                  <Typography className={classes.text}>
                    {state.product.base_product.brand.name.toUpperCase()}
                  </Typography>
                </Grid>
                <Grid container item direction="column">
                  <Typography className={classes.productInfoTitle}>
                    Modelo:
                  </Typography>
                  <Typography className={classes.text}>
                    {state.product.base_product.name}
                  </Typography>
                </Grid>
                <Grid container item direction="column">
                  <Typography className={classes.productInfoTitle}>
                    Marca:
                  </Typography>
                  <Typography className={classes.text}>
                    {state.product.base_product.brand.name}
                  </Typography>
                </Grid>
                <Grid container item direction="column">
                  <Typography className={classes.productInfoTitle}>
                    Descrição:
                  </Typography>
                  <Typography className={classes.text}>
                    {state.product.description}
                  </Typography>
                </Grid>
                <Grid container item spacing={2}>
                  {state.product.base_product.sub_categories.map(
                    subCategorie => (
                      <Grid item key={subCategorie.id}>
                        <Typography className={classes.subCategorie}>
                          {subCategorie.name}
                        </Typography>
                      </Grid>
                    )
                  )}
                </Grid>
                <Grid item>
                  <Button
                    className={[
                      classes.addButton,
                      session.cart.products.find(
                        p => state.product && p.id === state.product.id
                      )
                        ? classes.addedButton
                        : undefined
                    ].join(' ')}
                    onClick={
                      !session.cart.products.find(
                        p => state.product && p.id === state.product.id
                      )
                        ? session.addProductToCart(state.product)
                        : session.removeProductFromCart(state.product)
                    }
                  >
                    <Typography className={classes.text}>
                      {session.cart.products.find(
                        p => state.product && p.id === state.product.id
                      )
                        ? 'Adicionado ao carrinho'
                        : 'Adicionar ao carrinho'}
                    </Typography>
                  </Button>
                </Grid>
              </Grid>
              <Grid container item xs direction="column">
                <Grid item style={{ alignSelf: 'flex-end' }}>
                  <Typography className={classes.productInfoPrice}>
                    {state.product.price}€
                  </Typography>
                </Grid>
                <Grid item style={{ alignSelf: 'flex-end' }}>
                  <Typography className={classes.productInfoPriceRazored}>
                    {state.product.base_product.market_price}€
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          )
        )}
      </Grid>
    </div>
  );
};

export default withStyles(styles)(ProductInfo);
