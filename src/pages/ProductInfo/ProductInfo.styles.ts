import { createStyles } from '@material-ui/core';

export default createStyles({
  root: {
    height: '100vh',
    width: '100vw',
    overflow: 'hidden auto'
  },
  text: {
    fontSize: 'calc(10px + 0.5vmin)',
    textAlign: 'justify'
  },
  header: {
    padding: 5
  },
  headerLogo: {
    marginLeft: 20,
    cursor: 'pointer'
  },
  productInfoContainer: {
    backgroundColor: 'lightpink',
    borderRadius: 10,
    padding: 20
  },
  productInfoTitle: {
    fontWeight: 'bold',
    fontSize: 'calc(14px + 0.5vmin)',
    lineHeight: 2,
    letterSpacing: 0.8,
    color: 'chocolate'
  },
  productInfoPrice: {
    fontSize: 'calc(14px + 0.5vmin)',
    fontWeight: 'bold'
  },
  productInfoPriceRazored: {
    fontSize: 'calc(10px + 0.5vmin)',
    textDecoration: 'line-through'
  },
  image: {
    width: '100%',
    marginRight: 10
  },
  thumbnail: {
    height: 50,
    border: '1px solid black',
    marginRight: 5,
    cursor: 'pointer'
  },
  subCategorie: {
    backgroundColor: 'orange',
    padding: '0 10px',
    borderRadius: 5,
    width: 'max-content',
    marginTop: 10,
    color: 'white'
  },
  addButton: {
    marginTop: 15,
    borderRadius: 20,
    backgroundColor: 'black',
    color: 'white',
    width: '100%',
    '&:hover': {
      backgroundColor: 'black',
      opacity: 0.6
    },
    '& p': {
      fontSize: 'calc(10px + 0.5vmin)'
    }
  },
  addedButton: {
    backgroundColor: 'deeppink',
    '&:hover': {
      backgroundColor: 'hotpink'
    }
  }
});
