import { createStyles } from '@material-ui/core';

export default createStyles({
  root: {
    height: '100vh',
    width: '100vw',
    overflow: 'hidden auto'
  },
  text: {
    fontSize: 'calc(10px + 0.5vmin)'
  },
  header: {
    padding: 5
  },
  headerLogo: {
    marginLeft: 20,
    cursor: 'pointer'
  },
  textInput: {
    border: '1px solid black',
    borderRadius: 5,
    height: 40,
    '& .MuiInput-root': {
      height: '100%'
    }
  },
  searchAdornment: {
    paddingLeft: 10
  },
  shoppingCart: {
    position: 'absolute',
    padding: 20,
    width: 'max-content',
    backgroundColor: 'white',
    maxWidth: 500,
    top: 70,
    right: 8,
    zIndex: 1300,
    '-webkit-box-shadow': '0 0 5px 0 rgba(0,0,0,0.63)',
    '-moz-box-shadow': '0 0 5px 0 rgba(0,0,0,0.63)',
    boxShadow: '0 0 5px 0 rgba(0,0,0,0.63)'
  },
  shoppingCartBubble: {
    position: 'absolute',
    top: -5,
    right: -5,
    backgroundColor: 'lightpink',
    height: 15,
    width: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '50%'
  },
  shoppingCartBubbleText: {
    fontWeight: 'bold',
    fontSize: 'calc(6px + 0.5vmin)',
    color: 'black'
  },
  shoppingCartProduct: {
    border: '1px solid pink',
    padding: 10,
    borderRadius: 10,
    margin: 10
  },
  shoppingCartButton: {
    width: '200px',
    borderRadius: 20,
    color: 'white',
    backgroundColor: 'black',
    fontSize: 'calc(10px + 0.5vmin)',
    '&:hover': {
      backgroundColor: 'black'
    }
  },
  shoppingCartButtonReversed: {
    border: '3px solid black',
    backgroundColor: 'white',
    color: 'black',
    '&:hover': {
      backgroundColor: 'white'
    }
  },
  shoppingCartDeleteButton: {
    alignSelf: 'flex-end',
    cursor: 'pointer'
  },
  price: {
    fontSize: 'calc(14px + 0.5vmin)',
    fontWeight: 'bold'
  },
  priceRazored: {
    paddingLeft: 10,
    fontSize: 'calc(10px + 0.5vmin)',
    textDecoration: 'line-through'
  },
  timer: {
    backgroundColor: 'lightpink',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10
  },
  cartTextWrapper: {
    width: '200px',
    padding: 10,
    textAlign: 'center',
    borderBottom: '2px solid black'
  },
  cartText: {
    fontSize: 'calc(8px + 0.5vmin)',
    textTransform: 'uppercase'
  },
  title: {
    fontSize: 'calc(16px + 0.5vmin)'
  }
});
