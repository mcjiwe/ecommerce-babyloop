// React
import React, { useState, ChangeEvent } from 'react';

// Router
import { RouteComponentProps } from 'react-router';

// Material UI
import { withStyles, WithStyles } from '@material-ui/styles';

// Styles
import styles from './ShoppingCart.styles';
import { SessionContainer } from '../../stores/SessionStore';
import { Grid, TextField, Typography, Button } from '@material-ui/core';
import { Search, ShoppingCart, Delete } from '@material-ui/icons';

// Images
import Logo from '../../assets/Logo.png';

type Props = WithStyles<typeof styles> & RouteComponentProps;

const Home = ({ classes, ...props }: Props): JSX.Element => {
  const session = SessionContainer.useContainer();
  const [search, setSearch] = useState('');

  const handleChange = (event: ChangeEvent<any>): void => {
    const { value } = event.target;
    setSearch(value);
  };

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid container item xs justify="center" className={classes.header}>
          <Grid item xs>
            <img
              src={Logo}
              height={50}
              className={classes.headerLogo}
              onClick={(): void => props.history.push('/')}
              alt="Logo"
            />
          </Grid>
          <Grid container item xs={6}>
            <TextField
              name="search"
              value={search}
              onChange={handleChange}
              className={classes.textInput}
              fullWidth
              InputProps={{
                disableUnderline: true,
                startAdornment: <Search className={classes.searchAdornment} />
              }}
            />
          </Grid>
          <Grid
            container
            item
            xs
            alignItems="center"
            justify="center"
            style={{ position: 'relative' }}
          >
            <div style={{ position: 'relative' }}>
              <ShoppingCart />
              {session.cart.products.length > 0 && (
                <div className={classes.shoppingCartBubble}>
                  <Typography className={classes.shoppingCartBubbleText}>
                    {session.cart.products.length}
                  </Typography>
                </div>
              )}
            </div>
          </Grid>
        </Grid>
        {session.cart.products.length > 0 && (
          <Grid container item xs={12} className={classes.timer}>
            Tem{' '}
            {`${new Date(session.cart.countdown).getMinutes()}:${new Date(
              session.cart.countdown
            )
              .getSeconds()
              .toString()
              .padStart(2, '0')}`}{' '}
            para efetuar a compra.
          </Grid>
        )}
        <Grid container item style={{ padding: '40px 20% 0 20%' }}>
          <Grid item className={classes.cartTextWrapper}>
            <Typography className={classes.cartText}>1. Carrinho</Typography>
          </Grid>
          <Grid item xs={12} style={{ margin: '50px 0' }}>
            <Typography className={classes.title}>
              Confirme os produtos do seu carrinho
            </Typography>
          </Grid>
          <Grid container item spacing={4}>
            {session.cart.products.map(product => (
              <Grid
                key={product.id}
                container
                item
                spacing={1}
                className={classes.shoppingCartProduct}
              >
                <Grid item>
                  <img
                    src={product.product_pictures[0].picture.url}
                    width={100}
                    alt={product.base_product.name}
                  />
                </Grid>
                <Grid container item xs direction="column">
                  <Grid item>
                    <Typography className={classes.text}>
                      {product.base_product.name}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Typography className={classes.text}>
                      {product.base_product.brand.name}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container item xs justify="flex-end">
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      textAlign: 'end'
                    }}
                  >
                    <Typography className={classes.price}>
                      {product.price}
                    </Typography>
                    <Typography className={classes.priceRazored}>
                      {product.base_product.market_price}€
                    </Typography>
                    <Delete
                      onClick={session.removeProductFromCart(product)}
                      className={classes.shoppingCartDeleteButton}
                    />
                  </div>
                </Grid>
              </Grid>
            ))}
          </Grid>
          <Grid container item justify="flex-end" style={{ marginTop: '10px' }}>
            <Grid item xs={4}>
              <Typography className={classes.text}>Total</Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.text}>
                {session.cart.total.toFixed(2)}€
              </Typography>
            </Grid>
          </Grid>
          <Grid
            container
            item
            justify="space-between"
            style={{ marginTop: '50px' }}
          >
            <Button
              className={[
                classes.shoppingCartButton,
                classes.shoppingCartButtonReversed
              ].join(' ')}
              onClick={(): void => props.history.push('/')}
            >
              <Typography className={classes.text}>
                Continuar a comprar
              </Typography>
            </Button>
            <Button className={classes.shoppingCartButton}>
              <Typography className={classes.text}>Continuar</Typography>
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(styles)(Home);
