// React
import React from 'react';

// React router
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

// Pages
import Home from '../../pages/Home/Home';
import ProductInfo from '../../pages/ProductInfo/ProductInfo';
import ShoppingCart from '../../pages/ShoppingCart/ShoppingCart';

// Session Store
import { SessionContainer } from '../../stores/SessionStore';

export const Routes = (): JSX.Element => {
  return (
    <SessionContainer.Provider>
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={(props): JSX.Element => <Home {...props} />}
          />
          <Route
            exact
            path="/product/:id"
            render={(props): JSX.Element => <ProductInfo {...props} />}
          />
          <Route
            exact
            path="/shopping-cart"
            render={(props): JSX.Element => <ShoppingCart {...props} />}
          />
        </Switch>
      </Router>
    </SessionContainer.Provider>
  );
};
