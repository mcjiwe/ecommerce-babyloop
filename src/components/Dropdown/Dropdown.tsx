// React
import React from 'react';

// Material UI
import { withStyles, WithStyles } from '@material-ui/styles';
import { Select, Typography, MenuItem } from '@material-ui/core';

// Models
import { DropdownProps } from '../../models/Dropdown';

// Styles
import styles from './Dropdown.styles';

interface Props extends WithStyles<typeof styles> {
  name: string;
  placeholder: string;
  options: Array<DropdownProps>;
  selectedOption: number;
  handleChange: (event: React.ChangeEvent<{ id?: number; value: any }>) => void;
}

const Dropdown = ({ classes, ...props }: Props): JSX.Element => {
  return (
    <Select
      name={props.name}
      value={props.selectedOption}
      onChange={props.handleChange}
      className={classes.input}
      fullWidth
    >
      <MenuItem value={0}>
        <Typography>{props.placeholder}</Typography>
      </MenuItem>
      {props.options.map(option => (
        <MenuItem key={option.id} value={option.id}>
          <Typography>{option.value}</Typography>
        </MenuItem>
      ))}
    </Select>
  );
};

export default withStyles(styles)(Dropdown);
