import { createStyles } from '@material-ui/core';

export default createStyles({
  input: {
    width: 200,
    '& .MuiSelect-selectMenu': {
      marginLeft: 10,
      '& p': {
        fontSize: 'calc(10px + 0.5vmin)'
      }
    }
  }
});
