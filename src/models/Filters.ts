export interface FiltersProps {
  search: string;
  categoryId: number;
  brandId: number;
  subCategoryId: number;
  orderBy: number;
}
