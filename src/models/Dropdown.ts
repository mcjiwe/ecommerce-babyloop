export interface DropdownProps {
  id: number;
  value: string;
}
