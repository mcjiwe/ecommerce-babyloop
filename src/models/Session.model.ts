import { ProductProps } from './Product';

export interface SessionStateProps {
  cart: { products: Array<ProductProps>; total: number; countdown: number };
}

export interface SessionProps {
  cart: { products: Array<ProductProps>; total: number; countdown: number };
  addProductToCart: (product: ProductProps) => () => void;
  removeProductFromCart: (product: ProductProps) => () => void;
  getProducts: () => Promise<Array<ProductProps>>;
  getProduct: (productId: number) => Promise<ProductProps>;
}
