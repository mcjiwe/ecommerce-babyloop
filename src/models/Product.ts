export interface ProductProps {
  /**
   * Product id
   */
  id: number;
  /**
   * Creation date as a string
   */
  created_at: string;
  /**
   * Description of the product
   */
  description: string;
  /**
   * Product picture
   */
  product_pictures: Array<{ picture: { url: string } }>;
  /**
   * Product price
   */
  price: string;
  /**
   * Base product info
   */
  base_product: {
    id: number;
    name: string;
    description: string;
    market_price: string;
    model: string;
    status: number;
    volume: number;
    brand: {
      id: number;
      name: string;
      picture: { url: string };
    };
    categories: Array<{ id: number; name: string; picture: { url: string } }>;
    sub_categories: Array<{
      id: number;
      name: string;
      picture: { url: string };
    }>;
  };
}
