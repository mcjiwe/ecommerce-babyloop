// Axios
import axios from 'axios';

export default axios.create({
  baseURL: 'https://cors-anywhere.herokuapp.com/https://babyloop.pt/api/',
  timeout: 30000
});