// React
import React from 'react';

// Routes
import { Routes } from './components/Routes/Routes';

const App: React.FC = () => {
  return <Routes />;
};

export default App;
