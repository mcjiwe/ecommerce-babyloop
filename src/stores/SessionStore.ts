// React
import { useState } from 'react';

// Unstated
import { createContainer } from 'unstated-next';

// Axios
import axios from '../axios-config';

// Models
import { SessionProps, SessionStateProps } from '../models/Session.model';
import { ProductProps } from '../models/Product';

export const useSession = (): SessionProps => {
  const [session, setSession] = useState<SessionStateProps>({
    cart: { products: [], total: 0, countdown: 60 * 1000 * 10 }
  });

  const addProductToCart = (product: ProductProps) => (): void => {
    if (session.cart.products.length === 0) {
      setTimeout(
        () =>
          setSession({
            cart: { products: [], total: 0, countdown: 60 * 1000 * 10 }
          }),
        60 * 1000 * 10
      );
      setInterval(
        () =>
          setSession(prevState => ({
            cart: {
              ...prevState.cart,
              countdown: prevState.cart.countdown - 1000
            }
          })),
        1000
      );
    }

    const cartCopy = session.cart;
    cartCopy.total = cartCopy.total + parseFloat(product.price);
    cartCopy.products.push(product);
    setSession({
      cart: cartCopy
    });
  };

  const removeProductFromCart = (product: ProductProps) => (): void => {
    const cartCopy = session.cart;
    cartCopy.total = cartCopy.total - parseFloat(product.price);
    cartCopy.products = cartCopy.products.filter(p => p.id !== product.id);
    setSession({
      cart: cartCopy
    });
  };

  const getProducts = (): Promise<Array<ProductProps>> => {
    return axios.get(`v1/products`).then(response => response.data);
  };

  const getProduct = (productId: number): Promise<ProductProps> => {
    return axios
      .get(`v1/products/${productId}`)
      .then(response => response.data.product);
  };

  return {
    ...session,
    addProductToCart,
    removeProductFromCart,
    getProducts,
    getProduct
  };
};

export const SessionContainer = createContainer(useSession);
