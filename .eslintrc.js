module.exports = {
  // Specifies the ESLint parser
  "parser": "@typescript-eslint/parser",
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended",
    // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    "plugin:@typescript-eslint/recommended",
    // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    "prettier/@typescript-eslint",
    // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    "plugin:prettier/recommended"
  ],
  "parserOptions": {
    // Allows for the parsing of modern ECMAScript features
    "ecmaVersion": 2018,
    // Allows for the use of imports
    "sourceType": "module",
    // Allows for the parsing of JSX
    "ecmaFeatures": {
      "jsx": true
    },
    "project": "./tsconfig.json",
    "tsconfigRootDir": __dirname
  },
  "env": {
    "browser": true,
    "es6": true,
    "node": true,
    "jest": true
  },
  "plugins": [
    "react", "@typescript-eslint", "prettier", "react-hooks"
  ],
  "rules": {
    // Place to specify ESLint rules. Can be used to overwrite rules specified from the extended configs
    // e.g. "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn"
  },
  "settings": {
    // Auto detect react version being used
    "react": {
      "version": "detect"
    }
  }
}